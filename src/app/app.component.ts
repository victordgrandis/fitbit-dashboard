import { Component } from '@angular/core';
import { FitbitService } from './services/fitbit/fitbit.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public login: FitbitService) {
    this.login = login;
  }
}
