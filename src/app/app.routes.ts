import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/pages',
        pathMatch: 'full'
      },
      {
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule'
      }
    ]
  }
];
