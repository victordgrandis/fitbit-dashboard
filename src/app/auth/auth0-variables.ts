interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'fSEQWv2I8RoCJRzU5TtO7YIuyXYevhdl',
  domain: 'victor-dashboard.auth0.com',
  callbackURL: 'http://localhost:4200/'
};
