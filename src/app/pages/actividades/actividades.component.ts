import * as moment from 'moment'
import * as _ from 'lodash'
import nv from 'nvd3';
import d3 from 'd3';

import { Component, OnInit } from '@angular/core'

import { ActividadesService, RitmoCardiacoService } from 'app/services/service.index'
import { ActivatedRoute, Router } from '@angular/router'
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.component.html',
  styleUrls: ['./actividades.component.css']
})
export class ActividadesComponent implements OnInit {

  entrenamientos$;
  yogas$;
  columnas: string[] = ['select', 'fecha', 'duracion', 'calorias', 'ritmoPromedio']
  footerColumns: string[] = ['', '', 'duracion', 'calorias', 'ritmoPromedio']
  elementoExpandido;
  seleccionados: any[] = []
  entrenamiento: any
  entrenamientos: any
  grafico: any = {}
  grafico_cardiaco: any = {}
  graficos_cardiacos: any = {}
  public ritmo_promedio: number;
  public duracion_promedio: number;
  public calorias_promedio: number;
  public lineChartData: ChartDataSets[];
  public lineChartLabels: Label[];
  public lineChartOptions: any = {
     responsive: true,
  }
  public lineChartColors: Color[]= [
    {
      borderColor: 'rgba(255, 99, 132, 0.5)',
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      pointRadius: 0
    }, {
      borderColor: 'rgba(54, 162, 235, 0.5)',
      backgroundColor: 'rgba(54, 162, 235, 0.2)',
      pointRadius: 0
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  constructor(private _actividadesService: ActividadesService,
    private _ritmoCardiacoService: RitmoCardiacoService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.armarEntrenamiento(params['id'])
      } else if (params['id1'] && params['id2']) {
        this.compararEntrenamientos(params['id1'], params['id2'])
      } else {
        this.entrenamientos$ = this._actividadesService.obtenerPorTipo('Workout').toPromise().then((entrenamientos) => {
          this.ritmo_promedio = _.meanBy(entrenamientos, ent => ent.averageHeartRate);
          this.duracion_promedio = _.meanBy(entrenamientos, ent => ent.duracion);
          this.calorias_promedio = _.meanBy(entrenamientos, ent => ent.calories);
        });
        this.yogas$ = this._actividadesService.obtenerPorTipo('Yoga')
      }
    })
  }

  armarEntrenamiento(id) {
    this._actividadesService.obtenerPorId(id).subscribe((entrenamiento: any) => {
      this._ritmoCardiacoService.obtenerPorHoras(moment(entrenamiento.startTime).format('YYYY-MM-DD'),
        moment(entrenamiento.startTime).format('HH:mm'), moment(entrenamiento.startTime).add(entrenamiento.duration, 'ms')
          .format('HH:mm')).subscribe((ritmo) => {
            this.entrenamiento = entrenamiento
            this.entrenamiento.ritmo_cardiaco = ritmo
            this.lineChartData= [
              { data: _.map(ritmo['activities-heart-intraday'].dataset, 'value'), label: 'Ritmo cardíaco' },
            ];
            this.lineChartLabels = _.map(ritmo['activities-heart-intraday'].dataset, 'time');
          })
    })
  }

  compararEntrenamientos(id1, id2) {
    this._actividadesService.obtenerMultiplePorId(id1, id2).subscribe((entrenamientos: any) => {
      this.lineChartData = [];
      let prom = new Promise((resolve, reject) => {
        const length = entrenamientos.length;
        let sum = 0;
        _.forEach(entrenamientos, (entrenamiento, index) => {
          this._ritmoCardiacoService.obtenerPorHoras(moment(entrenamiento.startTime).format('YYYY-MM-DD'),
          moment(entrenamiento.startTime).format('HH:mm'), moment(entrenamiento.startTime).add(entrenamiento.duration, 'ms')
          .format('HH:mm')).subscribe((ritmo) => {
            entrenamiento.ritmo_cardiaco = ritmo

            this.lineChartData.push({ data: _.map(ritmo['activities-heart-intraday'].dataset, 'value'),
            label: 'Entrenamiento ' + (index + 1).toString() });
            if(+index == 0)
              this.lineChartLabels = _.map(ritmo['activities-heart-intraday'].dataset, 'time');
            sum++;
            if(sum == length) resolve();
          },() => reject())
        })
      });
      prom.then(() => this.entrenamientos = entrenamientos);
    })

  }

  verDetalle() {
    if (this.seleccionados.length === 1) {
      this.router.navigate(['/pages/actividades', this.seleccionados[0].logId])
    }
    if (this.seleccionados.length === 2) {
      this.router.navigate(['/pages/actividades', this.seleccionados[0].logId, this.seleccionados[1].logId])
    }
  }

  seleccionar(fila) {
    const elemento = _.remove(this.seleccionados, (selec) => {
      return selec.logId === fila.logId
    })
    if (elemento.length === 0) {
      this.seleccionados.push(fila)
      fila.seleccionado = true
    } else {
      fila.seleccionado = false
    }
  }
}
