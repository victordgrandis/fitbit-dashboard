import { Component, OnInit } from '@angular/core';
import { DatePipe, registerLocaleData } from '@angular/common';
import localEsAr from '@angular/common/locales/es-AR';
import { ActivatedRoute } from '@angular/router';
import { FitbitService } from '../../services/service.index';

registerLocaleData(localEsAr);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  actividades;
  pipe = new DatePipe('es-AR');

  constructor(public _fitBitService: FitbitService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['code']) {
        _fitBitService.getToken(params['code']).subscribe((response: any) => {
          localStorage.setItem('token', response.access_token);
        });
      }
    });
  }

  ngOnInit() {
    // this.cargarActividades();
  }

  cargarActividades() {
    const now = Date.now();
    const hoy = this.pipe.transform(now, 'short');
    this._fitBitService
      .getActividades(hoy)
      .subscribe(actividades => (this.actividades = actividades));
  }
}
