import { Component, OnInit } from '@angular/core';
import { DescansosService } from 'app/services/service.index';
import * as moment from 'moment';
import * as _ from 'lodash';
import nv from 'nvd3';
import d3 from 'd3';

@Component({
  selector: 'app-descanso',
  templateUrl: './descanso.component.html',
  styleUrls: ['./descanso.component.css']
})
export class DescansoComponent implements OnInit {

  descansos$; duracion;
  grafico_barras: any = {};
  grafico_linea: any = {};
  fecha_descanso: Date = new Date();

  constructor(private _descansoService: DescansosService) {
  }

  ngOnInit() {
    this.descansoPorFecha(this.fecha_descanso);
  }

  descansoPorFecha(fecha) {
    this.fecha_descanso = fecha;
    this.descansos$ = this._descansoService.obtenerPorFecha(moment(fecha).format('YYYY-MM-DD'));
    this.descansos$.toPromise().then((descansos) => {
      if (descansos.length > 0) {
        const duracion = moment.duration(descansos[0].duration);
        this.duracion = duracion.hours() + ' hs ' + duracion.minutes() + ' minutos';

        // gráfico de barras para el resumen
        const datos = [{
          key: 'Etapas de sueño',
          values: _.map(descansos[0].levels[0].summary, (value, key) => {
            return { label: key, value: value.minutes }
          })
        }];

        d3.selectAll('#chart svg').remove();

        nv.addGraph(function () {
          const width = 500;
          const height = 350;
          const chart = nv.models.discreteBarChart()
          .x(function (d) { return d.label })
          .y(function (d) { return d.value })
          .staggerLabels(true)
          .showValues(true)
          .width(width).height(height);

          d3.select('#chart svg')
          .datum(datos)
          .transition().duration(500)
          .call(chart)
          .attr('width', width)
          .attr('height', height);
          nv.utils.windowResize(chart.update);

          return chart;
        })
      }
    })
  }

}
