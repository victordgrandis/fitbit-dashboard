import { Component, OnInit } from '@angular/core';
import { FitbitService, ActividadesService, DescansosService, RitmoCardiacoService } from '../../services/service.index';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(public _fitBitService: FitbitService, public _actividadesService: ActividadesService, public _descansoService: DescansosService, public _ritmoCardiacoService: RitmoCardiacoService) {}

  ngOnInit() {
  }

  autorizar() {
    this._fitBitService
      .autorizar();
  }
  actualizarSueno() {
	  this._descansoService
	  	.actualizar().subscribe();
  }
  actualizarActividades() {
	  this._actividadesService
	  	.actualizar().subscribe();
  }
  actualizarRitmoCardiaco() {
	  this._ritmoCardiacoService
	  	.actualizar().subscribe();
  }
}
