import 'hammerjs';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PagesRoutes } from './pages.routing';
import { ChartsModule } from 'ng2-charts';

// Pipes

// Componentes personalizados
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DescansoComponent } from './descanso/descanso.component';
import { ActividadesComponent } from './actividades/actividades.component';
import { RitmoCardiacoComponent } from './ritmo-cardiaco/ritmo-cardiaco.component';

@NgModule({
  entryComponents: [],
  imports: [
    CommonModule,
    PagesRoutes,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule
  ],
  declarations: [HomeComponent, DashboardComponent, DescansoComponent, ActividadesComponent, RitmoCardiacoComponent]
})
export class PagesModule {}
