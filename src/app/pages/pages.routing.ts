import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DescansoComponent } from './descanso/descanso.component';
import { ActividadesComponent } from './actividades/actividades.component';
import { RitmoCardiacoComponent } from './ritmo-cardiaco/ritmo-cardiaco.component';

const pagesRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'home',
        component: HomeComponent,
        data: { titulo: 'Home', label: 'Home' }
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: { titulo: 'Dashboard', label: 'Dashboard' }
      },
      {
        path: 'descanso',
        component: DescansoComponent,
        data: { titulo: 'Descanso', label: 'Descanso' }
      },
      {
        path: 'actividades/:id',
        pathMatch: 'full',
        component: ActividadesComponent,
        data: { titulo: 'Actividades', label: 'Actividades' }
      },
      {
        path: 'actividades/:id1/:id2',
        pathMatch: 'full',
        component: ActividadesComponent,
        data: { titulo: 'Actividades', label: 'Actividades' }
      },
      {
        path: 'actividades',
        pathMatch: 'full',
        component: ActividadesComponent,
        data: { titulo: 'Actividades', label: 'Actividades' }
      },
      {
        path: 'ritmo-cardiaco',
        component: RitmoCardiacoComponent,
        data: { titulo: 'Ritmo Cardíaco', label: 'Ritmo Cardíaco' }
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(pagesRoutes)],
  exports: [RouterModule]
})
export class PagesRoutes {}
