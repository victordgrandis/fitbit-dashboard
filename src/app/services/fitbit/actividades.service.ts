import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators'
import * as moment from 'moment'
import * as _ from 'lodash'

@Injectable()
export class ActividadesService {

  url = 'http://localhost:3000/activities/'

  constructor(public http: HttpClient) {}

  actualizar() {
    return this.http.get(this.url + 'update')
  }

  obtenerPorTipo(tipo) {
    return this.http.get(this.url + 'getByType/' + tipo).pipe(map((data: any) => {
      _.forEach(data, element => {
        const duracion = moment.duration(element.duration)
        element.duracion = duracion.hours() + ' hs ' + duracion.minutes() + ' minutos'
        element.fecha = moment(element.startTime).format('DD-MM-YYYY')
      })
      return _.orderBy(data, 'startTime', 'desc')
    }))
  }

  obtenerPorId(id) {
    return this.http.get(this.url + 'getById/' + id).pipe(map((data: any) => {
      const duracion = moment.duration(data.duration)
      data.duracion = duracion.hours() + ' hs ' + duracion.minutes() + ' minutos'
      data.fecha = moment(data.startTime).format('DD-MM-YYYY')
      return data
    }))
  }

  obtenerMultiplePorId(id1, id2) {
    return this.http.get(this.url + 'getMultiById/' + id1 + '/' + id2).pipe(map((data: any) => {
      _.forEach(data, element => {
        const duracion = moment.duration(element.duration)
        element.duracion = duracion.hours() + ' hs ' + duracion.minutes() + ' minutos'
        element.fecha = moment(element.startTime).format('DD-MM-YYYY')
      })
      return data
    }))
  }
}
