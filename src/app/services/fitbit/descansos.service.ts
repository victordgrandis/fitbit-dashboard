import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class DescansosService {
	
	url = 'http://localhost:3000/sleep/';

	constructor(public http: HttpClient) {}
	
	actualizar() {
		return this.http.get(this.url + 'update');
	}
	obtenerPorFecha(fecha) {
		return this.http.get(this.url + 'getByDate/' + fecha);
	}
}
