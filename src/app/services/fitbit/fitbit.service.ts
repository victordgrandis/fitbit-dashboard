import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class FitbitService {

  user_id = '***';
  token;
  headers;

  constructor(
    public http: HttpClient,
  ) {
    this.token = localStorage.getItem('token');
    this.headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    this.headers = this.headers.append('Authorization', 'Bearer ' + this.token);
  }

  getToken(code) {
    let header_token = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    header_token = header_token.append('Authorization', 'Basic MjJEMlROOmIyNTM2Zjg2ZmIxOTgwMDJkYjUyNjlhMDFjZTkwYzI4')
    const data = 'clientId=' + this.user_id + '&grant_type=authorization_code&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2Fdashboard&code=' + code
    return this.http.post('https://api.fitbit.com/oauth2/token', data, {headers: header_token})
  }

  getActividades(date) {
    return this.http.get( 'https://api.fitbit.com/1/user/' + this.user_id + '/activities/date/' + date + '.json', {headers: this.headers} )
  }

  logout() {
    localStorage.removeItem('token');
  }
  
  login() {
    window.open('https://www.fitbit.com/oauth2/authorize?response_type=code' +
     '&client_id=' + this.user_id + '&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2Fdashboard' +
     '&scope=activity%20heartrate%20sleep&expires_in=604800')
  }

  autorizar() {
	window.open('http://localhost:3000/authorize');
  }
}
