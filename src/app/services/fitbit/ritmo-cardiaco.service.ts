import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class RitmoCardiacoService {
	
	url = 'http://localhost:3000/heart/';

	constructor(public http: HttpClient) {}
	
	actualizar() {
		return this.http.get(this.url + 'update');
	}
	
	obtenerPorHoras(fecha, hora_desde, hora_hasta) {
		return this.http.get(this.url + fecha + '/' + hora_desde + '/' + hora_hasta);
	}
}