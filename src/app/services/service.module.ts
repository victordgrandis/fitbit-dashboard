import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FitbitService, ActividadesService, DescansosService, RitmoCardiacoService } from './service.index';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [FitbitService, ActividadesService, DescansosService, RitmoCardiacoService],
  declarations: []
})
export class ServiceModule {}
