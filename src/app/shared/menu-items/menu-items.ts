import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [{
	state: 'home',
	name: 'Home',
    type: 'link',
	icon: 'home'
  },
  {
    state: 'pages',
    name: 'Dashboard',
    type: 'sub',
		icon: 'bubble_chart',
    children: [{
			state: 'descanso',
			name: 'Sueño',
			icon: 'bubble_chart',
	},{
		state: 'actividades',
		name: 'Actividades',
		icon: 'directions_run'
	},{
		state: 'ritmo-cardiaco',
		name: 'Corazón',
		icon: 'favorite'
	}]
  }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
