import { storiesOf, moduleMetadata } from '@storybook/angular';
import { HomeComponent } from '../app/pages/home/home.component';

storiesOf('Home component', module)
	.add('home with raised buttons', () => ({
		component: HomeComponent
	}))